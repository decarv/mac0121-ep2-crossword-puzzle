typedef struct {
	int line;
	int column;
	int dir;
	int windex;
	long unsigned int len;
	int filled;
	int intersects;
	int *intersections;
} position;

typedef struct {
	int line;
	int column;
	int words_size;
	int pos_size;
	position **pos;
	char **words;
	int *wordmask;
	int *posmask;
	int **matrix;
	char **board;
} instance;

int max(int, int);
void swap(instance *, int, int);
void ** defineboard(instance *);
void definematrix(instance *);
void definewords(instance *);
void printboard(instance *);
void mapboard(instance *);
void mapdir(instance *, int , int, int);
void storepos(instance *, int, int, int, int, int);
void mapintersections(instance *);
void storeposition(instance *, int, int, int, int, int);
void sortwords(instance *);
void sortpositions(instance *);
int fitposition(instance *, int);
int fitword(instance *, int);
void fillboard(instance *, int, int);
void unfillboard(instance *, int);
int solver(instance *);
void freeinstance(instance *);
