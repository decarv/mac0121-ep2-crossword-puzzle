#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "pilha.h"
#include "ep2.h"

#define HORIZONTAL 1
#define VERTICAL 0

int max(int a, int b) {
	if (a > b) return a;
	return b;
}

void swap(instance *inst, int i, int j) {
	char word[100];
	strcpy(word, inst->words[i]);
	strcpy(inst->words[i], inst->words[j]);
	strcpy(inst->words[j], word);
}

void ** defineboard(instance *inst) { 
	int i, j;
	inst->board = malloc(inst->line * sizeof(char *));
	for (i = 0; i < inst->line; i++) {
		inst->board[i] = malloc(inst->column * sizeof(char));
	}
	for (i = 0; i < inst->line; i++) {
		for (j = 0; j < inst->column; j++) {
			if (inst->matrix[i][j] == -1) {
				inst->board[i][j] = '*';
			}
			else {
				inst->board[i][j] = ' ';
			}
		}
	}
}

void definematrix(instance *inst) {
	int i, j;
	inst->matrix = malloc(inst->line * sizeof(int *));
	for (i = 0; i < inst->line; i++) {
		inst->matrix[i] = malloc(inst->column * sizeof(int));
		for (j = 0; j < inst->column; j++) {
			scanf("%d", &(inst->matrix[i][j]));
		}
	}
}

void definewords(instance *inst) {
	int i, j;
	scanf("%d", &(inst->words_size));
	inst->words = malloc(inst->words_size * sizeof(char *));
	
	for (i = 0; i < inst->words_size; i++) {
		inst->words[i] = malloc(max(inst->line, inst->column) * sizeof(char));
		scanf("%s", inst->words[i]);
	}
}

void printboard(instance *inst) {
	int i, j;
	for (i = 0; i < inst->line; i++) {
		for (j = 0; j < inst->column; j++) {
			printf("%c ", inst->board[i][j]);
		}
		printf("\n");
	}
}

/*
   Recebe uma instância e mapeia as posições livres do tabuleiro. As posições são
   guardadas pela função void storepos.
*/
void mapboard(instance * inst) {
	int i, j, ast;
	inst->pos_size = 0;
	inst->pos = malloc(inst->words_size * sizeof(position*));

	for (i = 0; i < inst->line; i++) {
		ast = -1;
		for (j = 0; j <	inst->column; j++) {
			if (inst->matrix[i][j] == -1) {
				if (j - ast > 2) {
					storepos(inst, inst->pos_size, i, j, ast, HORIZONTAL);
					inst->pos_size++;
				}
				ast = j;
			}
		}
		if (j - ast > 2) { 
			storepos(inst, inst->pos_size, i, j, ast, HORIZONTAL);
			inst->pos_size++;
		}
	}

	for (j = 0; j < inst->column; j++) {
		ast = -1;
		for (i = 0; i < inst->line; i++) {
			if (inst->matrix[i][j] == -1) {
				if (i - ast > 2) {
					storepos(inst, inst->pos_size, i, j, ast, VERTICAL);
					inst->pos_size++;
				}
				ast = i;
			}
		}
		if (i - ast > 2) {
			storepos(inst, inst->pos_size, i, j, ast, VERTICAL);
			inst->pos_size++;
		}
	}
}

void storepos(instance *inst, int k, int i, int j, int ast, int dir) {
	inst->pos[k] = malloc(sizeof(position));
	inst->pos[k]->filled = 0;
	inst->pos[k]->intersects = 0;
	
	if (dir == HORIZONTAL) {
		inst->pos[k]->line = i;
		inst->pos[k]->column = ast + 1;
		inst->pos[k]->len = j - (ast + 1);
		inst->pos[k]->dir = dir;
	}
	else {
		inst->pos[k]->line = ast + 1;
		inst->pos[k]->column = j;
		inst->pos[k]->len = i - (ast + 1);
		inst->pos[k]->dir = dir;
	}
}

/* 
   Recebe uma instância e mapeia todas as interseções de todas as palavras e as guarda 
   nos objetos position.
*/
void mapintersections(instance *inst) {
	int i, j;
	
	for (i = 0; i < inst->pos_size; i++) {
	inst->pos[i]->intersections = malloc(inst->pos_size * sizeof(int));
	   if (inst->pos[i]->dir == HORIZONTAL) {
		   for (j = 0; j < inst->pos_size; j++) {
			   if (inst->pos[j]->dir == VERTICAL) {
				   if (inst->pos[i]->line >= inst->pos[j]->line && \
					inst->pos[i]->line <= inst->pos[j]->line + inst->pos[j]->len - 1 && \
					inst->pos[i]->column <= inst->pos[j]->column && \
					inst->pos[i]->column + inst->pos[i]->len - 1 >= inst->pos[j]->column) {
					   inst->pos[i]->intersections[inst->pos[i]->intersects] = j;
					   inst->pos[i]->intersects++;
				   }
				}
			}
	   }
	   else {
		   for (j = 0; j < inst->pos_size; j++) {
			   if (inst->pos[j]->dir == HORIZONTAL) {
				   if (inst->pos[i]->line <= inst->pos[j]->line && \
				 	inst->pos[i]->line + inst->pos[i]->len - 1 >= inst->pos[j]->line && \
				   	inst->pos[i]->column >= inst->pos[j]->column && \
					inst->pos[i]->column <= inst->pos[j]->column + inst->pos[j]->len - 1) {
					   inst->pos[i]->intersections[inst->pos[i]->intersects] = j;
					   inst->pos[i]->intersects++;
					}
				}
		   }
	   }
	}
}

/* 
   Recebe uma instância e ordena a sequência de palavras da instância 
   em ordem decrescente de tamanho.
*/
void sortwords(instance *inst) {
	int i, k;

	k = 0;
	while (k < inst->words_size) {
		for (i = (k + 1); i < inst->words_size; i++) {
			if (strlen(inst->words[k]) < strlen(inst->words[i])) {
				swap(inst, k, i);
			}
		}
		k++;
	}
}

/* 
   Recebe uma instância e ordena a sequência de posições da instância 
   em ordem decrescente de tamanho.
*/
void sortpositions(instance *inst) {
	int i, k, n;
	position tmp;
	
	k = 0;
	while (k < inst->pos_size) {
		for (i = (k + 1); i < inst->pos_size; i++) {
			if (inst->pos[k]->len < inst->pos[i]->len) {
				tmp = *(inst->pos[i]);
				*(inst->pos[i]) = *(inst->pos[k]);
				*(inst->pos[k]) = tmp;
			}
		}
		k++;
	}
}

/* 
   Recebe um indice de posicao e retorna o indice da melhor palavra para encaixar na posicao,
   retorna -1 se não existe uma melhor palavra para a posicao, ou retorna -2 se não existe 
   qualquer palavra que encaixe na posição.
*/
int fitposition(instance *inst, int pindex) {
	int w, i, j, lin, col, p, windex, count;

	lin = inst->pos[pindex]->line;
	col = inst->pos[pindex]->column;
	count = 0;
	windex = -1;
	for (w = 0; w < inst->words_size; w++) {
		if (!inst->wordmask[w] && strlen(inst->words[w]) == inst->pos[pindex]->len) {
			if (inst->pos[pindex]->dir == HORIZONTAL) {
				for (i = 0; i < inst->pos[pindex]->len; i++) {
					if (inst->matrix[lin][col+i] > 0 && inst->board[lin][col+i] != inst->words[w][i]) break;
				}
				if (i == inst->pos[pindex]->len) {
					windex = w;
					count++;
					if (count > 1) return -1;
				}
			}
			else {
				for (i = 0; i < inst->pos[pindex]->len; i++) {
					if (inst->matrix[lin+i][col] > 0 && inst->board[lin+i][col] != inst->words[w][i]) break;
				}
				if (i == inst->pos[pindex]->len) {
					windex = w;
					count++;
					if (count > 1) return -1;
				}
			}
		}
	}
	if (count == 1) return windex;
	else return -2;
}

/* 
   Recebe um indice de palavra e retorna o indice da melhor posicao para encaixar a 
   palavra ou retorna -1 se a melhor posição não for encontrada.
*/
int fitword(instance *inst, int windex) {
	int j, p, lin, col, len, crossing, max, bestpos;
	
	max = -1;
	crossing = 0;
	bestpos = -1;
	for (p = 0; p < inst->pos_size; p++) {
		if (strlen(inst->words[windex]) == inst->pos[p]->len && !inst->pos[p]->filled) {
			crossing = 0;
			lin = inst->pos[p]->line;
			col = inst->pos[p]->column;
			len = inst->pos[p]->len; 
			if (inst->pos[p]->dir == HORIZONTAL) {
				for (j = 0; j < len; j++) {
					if (inst->matrix[lin][col+j] == 0) continue;
					else if (inst->board[lin][col+j] == inst->words[windex][j]) crossing++;
					else if (inst->board[lin][col+j] != inst->words[windex][j]) {
						crossing = -1;
						break;
					}
				}
			}
			else {
				for (j = 0; j < len; j++) {
					if (inst->matrix[lin+j][col] == 0) continue;
					else if(inst->board[lin+j][col] == inst->words[windex][j]) crossing++;
					else if (inst->board[lin+j][col] != inst->words[windex][j]) {
						crossing = -1;
						break;
					}
				}
			}
			if (crossing > max) {
				bestpos = p;
			}
		}
	}
	return bestpos;
}

/* 
   Recebe um indice de palavra (windex) e um indice de posicao (pindex), preenche a 
   posição no tabuleiro com a palavra e marca a matriz com os índices da palavra.
*/
void fillboard(instance * inst, int windex, int pindex) {
	int i, len, lin, col;
	
	len = inst->pos[pindex]->len;
	lin = inst->pos[pindex]->line;
	col = inst->pos[pindex]->column;
	if (inst->pos[pindex]->dir == HORIZONTAL) {
		for (i = 0; i < len; i++) {
			if (inst->matrix[lin][col+i] == 0) {
				inst->matrix[lin][col+i] = windex + 1;
				inst->board[lin][col+i] = inst->words[windex][i];
			}
		}
	}
	else {
		for (i = 0; i < len; i++) {
			if (inst->matrix[lin+i][col] == 0) {
				inst->matrix[lin+i][col] = windex + 1;
				inst->board[lin+i][col] = inst->words[windex][i];
			}
		}
	}
	inst->pos[pindex]->filled = 1;
	inst->pos[pindex]->windex = windex;
}

/* 
   Recebe um indice de palavra (windex) e despreenche a posição no tabuleiro que tem 
   a palavra e desmarca na matriz os indices da palavra.
*/
void unfillboard(instance *inst, int windex) {
	int i, j;
	for (i = 0; i < inst->pos_size; i++) {
		if (inst->pos[i]->windex == windex) {
			inst->pos[i]->filled = 0; 
		}
	}
	for (i = 0; i < inst->line; i++) {
		for (j = 0; j < inst->column; j++) {
			if (inst->matrix[i][j] == (windex + 1)) {
				inst->board[i][j] = ' ';
				inst->matrix[i][j] = 0;
			}
		}
	}
}

/* 
   Recebe uma instância e retorna 1 se foi possível resolver as palavras cruzadas ou
   retorna 0 se não foi possível resolver.
   O algoritmo encaixa palavras em posições livres e verifica se essa palavra pode permanecer
   no tabuleiro, considerando as palavras disponíveis e as intersecções da palavra encaixada.
   Se uma palavra não puder permanecer no tabuleiro, tira-se a palavra. Se uma palavra foi 
   encaixada incorretamente, o algoritmo faz backtracking para tentar outra sequência de 
   encaixes de palavras.
*/ 
int solver(instance *inst) {
	int i, k, windex, pindex, count, pfit, wfit, intersection, lastwordfit, attempt;
	pilha *stack;
	stack = criaPilha(inst->words_size);
	
	inst->wordmask = malloc(inst->words_size * sizeof(int));
	for (i = 0; i < inst->words_size; i++) inst->wordmask[i] = 0;
	
	windex = 0;
	count = 0;
	pindex = 0;

	while (count != inst->pos_size) {
		wfit = 0;
		while (!wfit && (windex < inst->words_size)) {
			if (inst->wordmask[windex] == 0) {
				pindex = fitword(inst, windex);
				if (pindex > -1) wfit = 1;
			}
			if (!wfit) windex++;
		}

		if (wfit) {
			inst->wordmask[windex] = 1;
			fillboard(inst, windex, pindex);
			empilha(stack, windex);
			attempt = windex;
			count++;
			/* O código abaixo testa as palavras que intersectam com a palavra encaixada.
		   	   Se alguma intersecção não puder ser preenchida, a palavra encaixada não deve
		       fazer parte da solução. */	   
			k = 0;
			for (i = 0; i < inst->pos[pindex]->intersects; i++) {
				pfit = 1;
				intersection = inst->pos[pindex]->intersections[i];
				if (inst->pos[intersection]->filled == 1) continue;
				else {
					windex = fitposition(inst, intersection);
					if (windex == -1) {
						windex = 0;
						continue;
					}
					else if (windex > -1) {
						fillboard(inst, windex, intersection);
						empilha(stack, windex);
						inst->wordmask[windex] = 1;
						windex = 0;
						count++;
						k++;
					}
					else { 
						pfit = 0;
						while (k--) { 
							i = desempilha(stack);
							unfillboard(inst, i);
							inst->wordmask[i] = 0;
							count--;
						}
						windex = desempilha(stack);
						unfillboard(inst, windex);
						count--;
						inst->wordmask[windex] = 0;
						windex++;
						break;
					}
				}
			}
			if (pfit) lastwordfit = attempt;
		}
		/* Caso não haja solução possível com a combinação de palavras testada, o código abaixo
		   faz backtracking. Retiram-se a palavra encaixada e as palavras que foram colocadas em
		   sua intersecção e passa-se ao próximo índice de palavra. */
		else {
			if (pilhaVazia(stack)) return 0;
			while (windex = desempilha(stack), windex != lastwordfit) {
				unfillboard(inst, windex);	
				inst->wordmask[windex] = 0;
			}
			unfillboard(inst, windex);
			inst->wordmask[windex] = 0;
			windex++;
			count--;
		}
	}
	destroiPilha(stack);
	return 1;
}

void freeinstance(instance *inst) {
	int i;
	
	if (inst->pos) {
		for (i = 0; i < inst->pos_size; i++) {
			if (inst->pos[i]->intersections) free(inst->pos[i]->intersections);
			inst->pos[i]->intersections = NULL;
		}
		for (i = 0; i < inst->pos_size; i++) {
			free(inst->pos[i]);
			inst->pos[i] = NULL;
		}
		free(inst->pos);
		inst->pos = NULL;
	}
	
	if (inst->words) {
		for (i = 0; i < inst->words_size; i++) {
			free(inst->words[i]);
			inst->words[i] = NULL;
		}
		free(inst->words);
		inst->words = NULL;
	}
	
	if (inst->matrix) {
		for (i = 0; i < inst->line; i++) {
			free(inst->matrix[i]);
			inst->matrix[i] = NULL;
		}
		free(inst->matrix);
		inst->matrix = NULL;	
	}
	
	if (inst->board) {
		for (i = 0; i < inst->line; i++) {
			free(inst->board[i]);
			inst->board[i] = NULL;
		}
		free(inst->board);
		inst->board = NULL;
	}
	
	if (inst->wordmask) free(inst->wordmask);
	inst->wordmask = NULL;
	
	free(inst);
	inst = NULL;
}

int main(void) {
	int i;
	instance *inst;
	inst = malloc(sizeof(instance));
	
	i = 1;
	while (scanf("%d %d", &(inst->line), &(inst->column)), (inst->line) || (inst->column)) {

		definematrix(inst);
		defineboard(inst);
		definewords(inst);
		sortwords(inst);
		mapboard(inst);
		sortpositions(inst);
		mapintersections(inst);
		
		printf("\nInstância %d\n", i);
		if (solver(inst)) {
			printboard(inst);
		}
		else {
			printf("Não há solução\n");
		}
		if (inst) freeinstance(inst);
		inst = malloc(sizeof(instance));
		i++;
	}
	if (inst) free(inst);
	inst = NULL;
	return 0;
}
